﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverScript : MonoBehaviour
{
    void OnGUI()
    {
        const int buttonWidth = 120;
        const int buttonHeight = 60;

        Rect buttonRestart = new Rect(
            Screen.width / 2 - buttonWidth / 2,
            1 * Screen.height / 3 - buttonHeight / 2,
            buttonWidth,
            buttonHeight
        );

        Rect buttonMenu = new Rect(
            Screen.width / 2 - buttonWidth / 2,
            2 * Screen.height / 3 - buttonHeight / 2,
            buttonWidth,
            buttonHeight
        );

        if (GUI.Button(buttonRestart, "Restart"))
        {
            Application.LoadLevel("Stage1");
        }

        if (GUI.Button(buttonMenu, "Menu"))
        {
            Application.LoadLevel("Menu");
        }
    }
}
