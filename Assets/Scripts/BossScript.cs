﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{
    private bool hasSpawn;
    
    private MoveScript moveScript;
    private Animator animator;
    private Collider2D collider;
    private WeaponScript[] weapons;
    private SpriteRenderer[] renderers;

    public float minAtackCooldown = 0.5f;
    public float maxAtackCooldown = 2.0f;

    private float aiCooldown;
    private bool isAtacking;
    private Vector2 positionTarget;

    void Awake()
    {
        animator = GetComponent<Animator>();
        moveScript = GetComponent<MoveScript>();
        collider = GetComponent<Collider2D>();
        weapons = GetComponentsInChildren<WeaponScript>();
        renderers = GetComponentsInChildren<SpriteRenderer>();
    }

    void Start()
    {
        hasSpawn = false;
        collider.enabled = false;
        moveScript.enabled = false;
        
        foreach (WeaponScript weapon in weapons)
        {
            weapon.enabled = false;
        }

        isAtacking = false;
        aiCooldown = maxAtackCooldown;
    }

    void Update()
    {
        if (hasSpawn == false)
        {
            if (renderers[0].IsVisibleFrom(Camera.main))
            {
                Spawn();
            }
        }
        else
        {
            aiCooldown -= Time.deltaTime;

            if (aiCooldown <= 0f)
            {
                isAtacking = !isAtacking;
                aiCooldown = Random.Range(maxAtackCooldown, maxAtackCooldown);
                positionTarget = Vector2.zero;

                animator.SetBool("Attack", isAtacking);
            }

            if (isAtacking)
            {
                moveScript.direction = Vector2.zero;

                foreach (WeaponScript weapon in weapons)
                {
                    if (weapon != null && weapon.enabled && weapon.CanAttack)
                    {
                        weapon.Attack(true);

                        SoundEffectsHelper.Instance.MakeEnemyShotSound();
                    }
                }
            }
            else
            {
                if (positionTarget == Vector2.zero)
                {
                    Vector2 randomPoint = new Vector2(Random.Range(0f, 1f), Random.Range(0f, 1f));
                    
                    positionTarget = Camera.main.ViewportToWorldPoint(randomPoint);
                }

                if (collider.OverlapPoint(positionTarget))
                {
                    positionTarget = Vector2.zero;
                }

                Vector3 direction = ((Vector3)positionTarget - this.transform.position);

                moveScript.direction = Vector3.Normalize(direction);
            }
        }
    }

    private void Spawn()
    {
        hasSpawn = true;

        collider.enabled = true;
        moveScript.enabled = true;

        foreach (WeaponScript weapon in weapons)
        {
            weapon.enabled = true;
        }

        foreach (ScrollingScript scrolling in FindObjectsOfType<ScrollingScript>())
        {
            if (scrolling.isLinkedToCamera)
            {
                scrolling.speed = Vector2.zero;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D otherCollider2D)
    {
        ShotScript shot = otherCollider2D.gameObject.GetComponent<ShotScript>();

        if (shot != null)
        {
            if (shot.isEnemyShot == false)
            {
                aiCooldown = Random.Range(minAtackCooldown, maxAtackCooldown);
                isAtacking = false;

                animator.SetTrigger("Hit");
            }
        }
    }

    void OnDrawGizmos()
    {
        if (hasSpawn && isAtacking == false)
        {
            Gizmos.DrawSphere(positionTarget, 0.25f);
        }
    }
}
