﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    private bool hasSpawn;
    private MoveScript moveScript;
    private WeaponScript[] weapons;

    // Start is called before the first frame update
    void Awake()
    {
        weapons = GetComponentsInChildren<WeaponScript>();

        moveScript = GetComponent<MoveScript>();
    }

    void Start()
    {
        hasSpawn = false;

        this.GetComponent<Collider2D>().enabled = false;
        moveScript.enabled = false;

        foreach (WeaponScript weapon in weapons)
        {
            weapon.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (hasSpawn == false)
        {
            if (GetComponent<Renderer>().IsVisibleFrom(Camera.main))
            {
                Spawn();
            }
        }
        else
        {
            foreach (WeaponScript weapon in weapons)
            {
                if (weapon != null && weapon.CanAttack)
                {
                    weapon.Attack(true);

                    SoundEffectsHelper.Instance.MakeEnemyShotSound();
                }
            }

            if (GetComponent<Renderer>().IsVisibleFrom(Camera.main) == false)
            {
                Destroy(gameObject);
            }
        }
    }

    private void Spawn()
    {
        hasSpawn = true;

        this.GetComponent<Collider2D>().enabled = true;
        moveScript.enabled = true;

        foreach (WeaponScript weapon in weapons)
        {
            weapon.enabled = true;
        }
    }
}
